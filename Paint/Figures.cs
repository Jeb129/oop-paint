﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Paint
{
    [Serializable]
    public class Figure
    {
        protected float _width;
        protected Color _color;

        public List<Figure> figures = new List<Figure>();
        public List<Point> points = new List<Point>();

        public Figure() { }

        protected Figure(Pen pen)
        {
            _color = pen.Color;
            _width = pen.Width;
        }

        protected int[] GetParametrs()
        {
            int[] result = new int[4];

            result[0] = Math.Min(points[0].X, points[^1].X);
            result[1] = Math.Min(points[0].Y, points[^1].Y);
            result[2] = Math.Abs(points[^1].X - points[0].X);
            result[3] = Math.Abs(points[^1].Y - points[0].Y);

            return result;
        }
        public virtual void Draw(Graphics g)
        {
            foreach (var figure in figures)
                if (figure != null)
                    figure.Draw(g);
        }
    }

    [Serializable]
    public class Rectangle: Figure
    {
        public Rectangle(Pen pen) : base(pen) { }

        public override void Draw(Graphics g)
        {
            int[] param = GetParametrs();
            System.Drawing.Rectangle rect = new(param[0], param[1], param[2], param[3]);
            g.DrawRectangle(new Pen(_color, _width), rect);      
        }
    }

    [Serializable]
    public class Ellipse: Figure 
    {
        public Ellipse(Pen pen) : base(pen) { }
        public override void Draw(Graphics g)
        {
            int[] param = GetParametrs();
            System.Drawing.Rectangle rect = new(param[0], param[1], param[2], param[3]);
            g.DrawEllipse(new Pen(_color, _width), rect);
        }
    }

    [Serializable]
    public class Line: Figure
    {
        public Line(Pen pen) : base (pen) { }
        public override void Draw(Graphics g) =>
           g.DrawLines(new Pen(_color,_width),points.ToArray());
    }

    [Serializable]
    public class Straightline : Figure
    {
        public Straightline(Pen pen) : base(pen) { }
        public override void Draw(Graphics g) => 
            g.DrawLine(new Pen(_color, _width), points.ToArray()[0], points.ToArray()[^1]);
    }
}
