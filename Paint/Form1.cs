﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Paint
{
    public partial class Form1 : Form
    {
        private Point _old;

        private delegate void DelegatePaint(Point old, Point current);
        private delegate void DelegateCreate();
        private DelegateCreate _create;
        private DelegatePaint _paint;

        private Graphics _g;
        private Pen _pen;

        private Rectangle _rectangle;
        private Ellipse _ellipse;
        private Line _line;
        private Straightline _stLine;
        private Figure _figures;

        bool _isSaved = true; // 

        private bool _isDraw = false; //флаг для определения, рисуем или нет
        public Form1()
        {
            InitializeComponent();
            _old = new Point();
            _figures = new Figure();
            _g = canvas_panel.CreateGraphics();
            _g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias; //задаем качество отрисовки
            _pen = new Pen(Color.Black, 0);
            _pen.StartCap = _pen.EndCap = System.Drawing.Drawing2D.LineCap.Round; //задаем стиль линий

            _paint = Drawing;
            _create = CreateLine;
            _pen.Color = pic_color.BackColor;
        }
        #region Ядро
        private void Canvas_panel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _isDraw = true;
                _create?.Invoke();//проверка на null
                _old = e.Location;
                if (_paint != null)
                    _isSaved = false;
            }
        }
        private void Canvas_panel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (_paint == Drawing)
                {
                    _paint(_old, e.Location);
                    _old = e.Location;
                }
                else
                    _paint?.Invoke(_old, e.Location);
            }
        }
        private void Canvas_panel_MouseUp(object sender, MouseEventArgs e)
        {
            _isDraw = false;
            _paint?.Invoke(_old, e.Location);
        }
        #endregion
        #region Инструменты
        private void Btn_pencil_Click(object sender, EventArgs e) //кнопка карандаша
        {
            _paint = Drawing;
            _create = CreateLine;
        }

        private void Btn_color_Click(object sender, EventArgs e) //выбор цвета
        {
            ColorDialog cd = new ColorDialog();
            if (cd.ShowDialog() == DialogResult.OK)
            {
                _pen.Color = cd.Color;
                pic_color.BackColor = cd.Color;
            }
        }

        private void Btn_rectangle_Click(object sender, EventArgs e) //прямоугольник
        {
            _paint = DrawRectangle;
            _create = CreateRectangle;
        }

        private void Btn_ellipse_Click(object sender, EventArgs e) //кружок
        {
            _paint = DrawEllipse;
            _create = CreateEllipse;
        }

        private void Btn_line_Click(object sender, EventArgs e) //прямая линия
        {
            _paint = DrawStLine;
            _create = CreateStLine;
        }

        private void Btn_back_Click(object sender, EventArgs e) //отмена действия
        {
            if (_figures.figures.Count != 0)
                _figures.figures.RemoveAt(_figures.figures.Count - 1);
            Refresh(); //перерисовка 
        }

        private void trackBar2_Scroll(object sender, EventArgs e) => //толщина кисти
            _pen.Width = (float)trackBar2.Value;
        #endregion
        #region Рисование
        private void DrawRectangle(Point old, Point current) =>
            DrawShape(_rectangle, old, current);

        private void DrawEllipse(Point old, Point current) =>
            DrawShape(_ellipse, old, current);

        private void DrawStLine(Point old, Point current) =>
            DrawShape(_stLine, old, current);

        void DrawShape(Figure f, Point old, Point current)
        {
            f.points.Add(old);
            if (!_isDraw)
            {
                f.points.Add(current);
                f.Draw(_g);
                _figures.figures.Add(f);
            }
        }

        // Обычное рисование
        private void Drawing(Point old, Point current)
        {
            _line.points.Add(old);
            _line.points.Add(current);
            _line.Draw(_g);
            if (!_isDraw)
                _figures.figures.Add(_line);
        }
        #endregion
        #region Создание фигур
        private void CreateLine() => _line = new Line(_pen);

        public void CreateRectangle() => _rectangle = new Rectangle(_pen);

        public void CreateEllipse() => _ellipse = new Ellipse(_pen);

        public void CreateStLine() => _stLine = new Straightline(_pen);
        #endregion
        #region Очистка/сохранение
        private void Btn_clear_Click(object sender, EventArgs e) //полная очистка
        {
            if (_figures.figures.Count == 0) return;
            var result = MessageBox.Show("Очистить холст?\nЭто действие необратимо",
                "Очистка",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (result != DialogResult.Yes) return;
            _figures.figures.Clear();
            Refresh();
            _isSaved = false;
        }
        private void Savebutton_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.InitialDirectory = "d:\\";
                sfd.Filter = "MyPaint  (*mpf) | *.mpf";
                sfd.RestoreDirectory = true;
                sfd.DefaultExt = "mpf";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    Stream stream = sfd.OpenFile();
                    if (stream != null)
                    {
                        BinaryFormatter myBin = new BinaryFormatter();
                        myBin.Serialize(stream, _figures.figures); //Сериализует объект  с указанной вершиной в заданный поток.
                        stream.Close();
                    }
                }
            }
            _isSaved = true;
        }
        private void OpenButton_Click(object sender, EventArgs e)
        {
            if (!_isSaved)
            {
                var result = MessageBox.Show("Открытие нового холста удалит текущий\nСохранить текщий холст?",
                    "Открытие",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                    Savebutton_Click(sender, e);
                else if (result == DialogResult.Cancel)
                    return;
            }
            using (OpenFileDialog openFile = new OpenFileDialog())
            {
                openFile.InitialDirectory = "d:\\";
                openFile.Filter = "Файлы MyPaint  (*mpf) | *.mpf";
                openFile.RestoreDirectory = true;
                if (openFile.ShowDialog() == DialogResult.OK)
                {
                    if (_figures.figures.Count != 0) //очищаем то, что нарисовали ранее
                        _figures.figures.Clear();
                    Refresh();
                    Stream mystr = openFile.OpenFile();
                    if (mystr != null)
                    {
                        try
                        {
                            BinaryFormatter myBin = new BinaryFormatter();
                            _figures.figures = (List<Figure>)myBin.Deserialize(mystr);
                            mystr.Close();
                            _figures.Draw(_g);
                        }
                        catch
                        {
                            MessageBox.Show("Ошибка при открытии файла");
                        }
                    }
                }
            }
            _paint = null;
            _isSaved = true;
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_isSaved)
            {
                var result = MessageBox.Show("Вы не сохранили изменения\nСохранить текщий холст?",
                    "Открытие",
                    MessageBoxButtons.YesNoCancel,
                    MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                    Savebutton_Click(sender, e);
                else if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
        }
        #endregion
        private void Canvas_panel_Paint(object sender, PaintEventArgs e)
        {
            _g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            _figures.Draw(_g);
        }
        private void Form1_Resize(object sender, EventArgs e) =>
            _g = canvas_panel.CreateGraphics();
    }
}
