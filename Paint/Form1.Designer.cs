﻿
namespace Paint
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            canvas_panel = new System.Windows.Forms.Panel();
            panel1 = new System.Windows.Forms.Panel();
            OpenButton = new System.Windows.Forms.Button();
            Savebutton = new System.Windows.Forms.Button();
            panel4 = new System.Windows.Forms.Panel();
            label3 = new System.Windows.Forms.Label();
            trackBar2 = new System.Windows.Forms.TrackBar();
            panel3 = new System.Windows.Forms.Panel();
            label2 = new System.Windows.Forms.Label();
            btn_ellipse = new System.Windows.Forms.Button();
            btn_rectangle = new System.Windows.Forms.Button();
            button1 = new System.Windows.Forms.Button();
            panel2 = new System.Windows.Forms.Panel();
            pic_color = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            btn_pencil = new System.Windows.Forms.Button();
            button2 = new System.Windows.Forms.Button();
            button3 = new System.Windows.Forms.Button();
            panel1.SuspendLayout();
            panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)trackBar2).BeginInit();
            panel3.SuspendLayout();
            panel2.SuspendLayout();
            SuspendLayout();
            // 
            // canvas_panel
            // 
            canvas_panel.BackColor = System.Drawing.Color.White;
            canvas_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            canvas_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            canvas_panel.Location = new System.Drawing.Point(0, 0);
            canvas_panel.Name = "canvas_panel";
            canvas_panel.Size = new System.Drawing.Size(1255, 695);
            canvas_panel.TabIndex = 2;
            canvas_panel.Paint += Canvas_panel_Paint;
            canvas_panel.MouseDown += Canvas_panel_MouseDown;
            canvas_panel.MouseMove += Canvas_panel_MouseMove;
            canvas_panel.MouseUp += Canvas_panel_MouseUp;
            // 
            // panel1
            // 
            panel1.BackColor = System.Drawing.Color.White;
            panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel1.Controls.Add(OpenButton);
            panel1.Controls.Add(Savebutton);
            panel1.Controls.Add(panel4);
            panel1.Controls.Add(panel3);
            panel1.Controls.Add(panel2);
            panel1.Controls.Add(button3);
            panel1.Dock = System.Windows.Forms.DockStyle.Left;
            panel1.Location = new System.Drawing.Point(0, 0);
            panel1.Margin = new System.Windows.Forms.Padding(10);
            panel1.Name = "panel1";
            panel1.Size = new System.Drawing.Size(255, 695);
            panel1.TabIndex = 3;
            // 
            // OpenButton
            // 
            OpenButton.BackColor = System.Drawing.Color.Transparent;
            OpenButton.Cursor = System.Windows.Forms.Cursors.Hand;
            OpenButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            OpenButton.FlatAppearance.BorderSize = 5;
            OpenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            OpenButton.Location = new System.Drawing.Point(69, 580);
            OpenButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            OpenButton.Name = "OpenButton";
            OpenButton.Size = new System.Drawing.Size(127, 84);
            OpenButton.TabIndex = 17;
            OpenButton.Text = "Открыть";
            OpenButton.UseVisualStyleBackColor = false;
            OpenButton.Click += OpenButton_Click;
            // 
            // Savebutton
            // 
            Savebutton.BackColor = System.Drawing.Color.Transparent;
            Savebutton.Cursor = System.Windows.Forms.Cursors.Hand;
            Savebutton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            Savebutton.FlatAppearance.BorderSize = 5;
            Savebutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            Savebutton.Location = new System.Drawing.Point(69, 488);
            Savebutton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Savebutton.Name = "Savebutton";
            Savebutton.Size = new System.Drawing.Size(127, 84);
            Savebutton.TabIndex = 16;
            Savebutton.Text = "Сохранить";
            Savebutton.UseVisualStyleBackColor = false;
            Savebutton.Click += Savebutton_Click;
            // 
            // panel4
            // 
            panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel4.Controls.Add(label3);
            panel4.Controls.Add(trackBar2);
            panel4.Cursor = System.Windows.Forms.Cursors.Hand;
            panel4.Dock = System.Windows.Forms.DockStyle.Top;
            panel4.Location = new System.Drawing.Point(0, 266);
            panel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            panel4.Name = "panel4";
            panel4.Size = new System.Drawing.Size(253, 133);
            panel4.TabIndex = 15;
            // 
            // label3
            // 
            label3.Dock = System.Windows.Forms.DockStyle.Top;
            label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label3.Location = new System.Drawing.Point(0, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(251, 40);
            label3.TabIndex = 8;
            label3.Text = "Толщина";
            label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // trackBar2
            // 
            trackBar2.Location = new System.Drawing.Point(5, 58);
            trackBar2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            trackBar2.Maximum = 15;
            trackBar2.Name = "trackBar2";
            trackBar2.Size = new System.Drawing.Size(240, 56);
            trackBar2.TabIndex = 0;
            trackBar2.Scroll += trackBar2_Scroll;
            // 
            // panel3
            // 
            panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel3.Controls.Add(label2);
            panel3.Controls.Add(btn_ellipse);
            panel3.Controls.Add(btn_rectangle);
            panel3.Controls.Add(button1);
            panel3.Dock = System.Windows.Forms.DockStyle.Top;
            panel3.Location = new System.Drawing.Point(0, 133);
            panel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            panel3.Name = "panel3";
            panel3.Size = new System.Drawing.Size(253, 133);
            panel3.TabIndex = 14;
            // 
            // label2
            // 
            label2.Dock = System.Windows.Forms.DockStyle.Top;
            label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label2.Location = new System.Drawing.Point(0, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(251, 40);
            label2.TabIndex = 8;
            label2.Text = "Фигуры";
            label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btn_ellipse
            // 
            btn_ellipse.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn_ellipse.Cursor = System.Windows.Forms.Cursors.Hand;
            btn_ellipse.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            btn_ellipse.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            btn_ellipse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn_ellipse.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            btn_ellipse.ForeColor = System.Drawing.Color.Black;
            btn_ellipse.Image = Properties.Resources.circle;
            btn_ellipse.Location = new System.Drawing.Point(179, 43);
            btn_ellipse.Name = "btn_ellipse";
            btn_ellipse.Size = new System.Drawing.Size(53, 61);
            btn_ellipse.TabIndex = 7;
            btn_ellipse.UseVisualStyleBackColor = true;
            btn_ellipse.Click += Btn_ellipse_Click;
            // 
            // btn_rectangle
            // 
            btn_rectangle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn_rectangle.Cursor = System.Windows.Forms.Cursors.Hand;
            btn_rectangle.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            btn_rectangle.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            btn_rectangle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn_rectangle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point);
            btn_rectangle.ForeColor = System.Drawing.Color.Black;
            btn_rectangle.Image = Properties.Resources.rectangle;
            btn_rectangle.Location = new System.Drawing.Point(100, 43);
            btn_rectangle.Name = "btn_rectangle";
            btn_rectangle.Size = new System.Drawing.Size(53, 61);
            btn_rectangle.TabIndex = 4;
            btn_rectangle.UseVisualStyleBackColor = true;
            btn_rectangle.Click += Btn_rectangle_Click;
            // 
            // button1
            // 
            button1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            button1.Cursor = System.Windows.Forms.Cursors.Hand;
            button1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            button1.ForeColor = System.Drawing.SystemColors.ControlText;
            button1.Image = Properties.Resources.line;
            button1.Location = new System.Drawing.Point(18, 43);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(53, 61);
            button1.TabIndex = 10;
            button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            button1.UseVisualStyleBackColor = true;
            button1.Click += Btn_line_Click;
            // 
            // panel2
            // 
            panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            panel2.Controls.Add(pic_color);
            panel2.Controls.Add(label1);
            panel2.Controls.Add(btn_pencil);
            panel2.Controls.Add(button2);
            panel2.Dock = System.Windows.Forms.DockStyle.Top;
            panel2.Location = new System.Drawing.Point(0, 0);
            panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            panel2.Name = "panel2";
            panel2.Size = new System.Drawing.Size(253, 133);
            panel2.TabIndex = 13;
            // 
            // pic_color
            // 
            pic_color.Anchor = System.Windows.Forms.AnchorStyles.Left;
            pic_color.BackColor = System.Drawing.Color.Black;
            pic_color.Cursor = System.Windows.Forms.Cursors.Hand;
            pic_color.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            pic_color.ForeColor = System.Drawing.Color.White;
            pic_color.Location = new System.Drawing.Point(8, 63);
            pic_color.Name = "pic_color";
            pic_color.Size = new System.Drawing.Size(45, 45);
            pic_color.TabIndex = 1;
            pic_color.UseVisualStyleBackColor = false;
            pic_color.Click += Btn_color_Click;
            // 
            // label1
            // 
            label1.Dock = System.Windows.Forms.DockStyle.Top;
            label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            label1.Location = new System.Drawing.Point(0, 0);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(251, 40);
            label1.TabIndex = 8;
            label1.Text = "Инструменты";
            label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btn_pencil
            // 
            btn_pencil.Anchor = System.Windows.Forms.AnchorStyles.Left;
            btn_pencil.BackgroundImage = (System.Drawing.Image)resources.GetObject("btn_pencil.BackgroundImage");
            btn_pencil.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            btn_pencil.Cursor = System.Windows.Forms.Cursors.Hand;
            btn_pencil.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            btn_pencil.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            btn_pencil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btn_pencil.ForeColor = System.Drawing.SystemColors.ControlText;
            btn_pencil.Location = new System.Drawing.Point(100, 63);
            btn_pencil.Name = "btn_pencil";
            btn_pencil.RightToLeft = System.Windows.Forms.RightToLeft.No;
            btn_pencil.Size = new System.Drawing.Size(45, 45);
            btn_pencil.TabIndex = 3;
            btn_pencil.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            btn_pencil.UseVisualStyleBackColor = true;
            btn_pencil.Click += Btn_pencil_Click;
            // 
            // button2
            // 
            button2.Cursor = System.Windows.Forms.Cursors.Hand;
            button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            button2.Image = (System.Drawing.Image)resources.GetObject("button2.Image");
            button2.Location = new System.Drawing.Point(187, 63);
            button2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(45, 45);
            button2.TabIndex = 11;
            button2.UseVisualStyleBackColor = true;
            button2.Click += Btn_back_Click;
            // 
            // button3
            // 
            button3.BackColor = System.Drawing.Color.Transparent;
            button3.Cursor = System.Windows.Forms.Cursors.Hand;
            button3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            button3.FlatAppearance.BorderSize = 5;
            button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            button3.Location = new System.Drawing.Point(69, 408);
            button3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            button3.Name = "button3";
            button3.Size = new System.Drawing.Size(127, 72);
            button3.TabIndex = 12;
            button3.Text = "Очистить";
            button3.UseVisualStyleBackColor = false;
            button3.Click += Btn_clear_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            ClientSize = new System.Drawing.Size(1255, 695);
            Controls.Add(panel1);
            Controls.Add(canvas_panel);
            Name = "Form1";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "Form1";
            FormClosing += Form1_FormClosing;
            Resize += Form1_Resize;
            panel1.ResumeLayout(false);
            panel4.ResumeLayout(false);
            panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)trackBar2).EndInit();
            panel3.ResumeLayout(false);
            panel2.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private System.Windows.Forms.Panel canvas_panel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.Button Savebutton;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_ellipse;
        private System.Windows.Forms.Button btn_rectangle;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button pic_color;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_pencil;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
    }
}

